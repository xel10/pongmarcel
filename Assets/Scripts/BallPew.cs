﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallPew : MonoBehaviour {

	public Vector2 speed;

	private Vector2 iniSpeed;
	public Vector3 iniPos;

	// Use this for initialization
	void Start () 
	{
		iniSpeed = speed;
		iniPos = transform.position;
	}

	// Update is called once per frame
	void Update () 
	{
		transform.Translate (speed.x*Time.deltaTime, speed.y*Time.deltaTime, 0);
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Parets") 
		{
			speed.y *= -1f;
		}
		if (other.tag == "Paretsy") 
		{
			speed.x *= -1f;
		} 
		else if (other.tag == "Goal") 
		{
			speed.y *= -1f;
		}
	}

	void Reset()
	{
		transform.position = iniPos;
		speed = iniSpeed;
	}
}
